#mpiexec -np 16 ../bin/elecfem3d_mpi_optimized.ompi.314 elecfem3d_mpi.pam
#mpiexec -np 16 ../bin/elecfem3d_mpi_optimized.ompi.314 elecfem3d_mpi_512.pam CG1M1_512.raw
ulimit -s unlimited
mpiexec -np 16 ../bin/elecfem3d_mpi_optimized.intel elecfem3d_mpi_512.pam CG1M1_512.raw
#mpiexec -np 16 ../bin/elecfem3d_mpi_optimized.intel elecfem3d_mpi.pam
