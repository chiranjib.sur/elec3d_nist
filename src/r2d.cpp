
//==============================================================================//

// Program to convert RAW file to DAT format
// Developed by : Calligotech under contract from Shell
//
// For details contact : Chiranjib.Sur@shell.com
// Last Update: September 3, 2020
//
// (c) : Shell International, 2020

//==============================================================================//


//==============================================================================//
// System headers
//==============================================================================//

#include <cstring>
#include <fstream>
#include <iostream>
#include <regex>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

//==============================================================================//

 
//==============================================================================//
void displayUsagePattern( char *argv )
{
	std::cout << " Usage " << argv << " -f [.raw File Name]" << std::endl; 
}

//==============================================================================//

void displayHelp( char *argv )
{
	displayUsagePattern(argv);
}

//==============================================================================//

void stripExtension( std::string &path )
{
	size_t found;

	found = path.rfind(".raw");
	if(found != std::string::npos) 
	{
		path.resize(found);
	}
}

//==============================================================================//

void processFile( char *fName )
{
	std::string inFile, outFile;
	char *buffer, *outBuf, ch = 0x0A;
	int i,blksize,iteration,extra;
	long cRead;
	std::ifstream inF;
	std::ofstream outF;
	struct stat fi;
	
    // Reading the Block size of the file system
	stat("/", &fi);
	blksize = fi.st_blksize;

    // Allocating Filename with extension .raw and .dat
	inFile  = new char[strlen(fName)+5];
	outFile = new char[strlen(fName)+5];

	inFile  = outFile = fName;
	
	stripExtension(inFile);
	stripExtension(outFile); 
	inFile.append(".raw\0");
	outFile.append(".dat\0");

	inF.open(inFile, std::ios::out | std::ios::binary);
	if(!inF) 
	{
		std::cout << "Error opening the " << inFile << "File" << std::endl;
		return;
	}
	outF.open(outFile,std::ios::out | std::ios::binary);
	if(!outF) 
	{
		std::cout << "Error opening the " << outFile << "File" << std::endl;
		return;
	} 

	if(inF.is_open()) 
	{
		inF.seekg(0,inF.end);
		cRead = inF.tellg();
		inF.seekg(0,inF.beg);
		iteration = cRead / blksize;
		extra = cRead - ((long)iteration * blksize);
		try 
		{
			buffer = new char[blksize];
		}
		catch(std::bad_alloc&) 
		{
			std::cout << "Error allocating memory for input Buffer " << std::endl;
			std::cout << "Exiting..." << std::endl;
			return;
		}
		try 
		{
			outBuf = new char[blksize * 2 + 1];
		}
		catch(std::bad_alloc&) 
		{
			std::cout << "Error allocating memory for the output Buffer " << std::endl;
			std::cout << "Exiting..." << std::endl;
			return;
		}
		while(iteration--) 
		{
			inF.read(buffer,blksize);
			memset(outBuf,ch,blksize*2);
			for(i=0;i<blksize;i++) 
			{
				outBuf[i*2] = buffer[i] + 48;
			}
			outF.write(outBuf,blksize * 2);
		}

		inF.read(buffer,extra);		// Reading the extra bytes at the end
		memset(outBuf,ch,extra*2);
		for(i=0;i<extra;i++) 
		{
			outBuf[i*2] = buffer[i] + 48;
		}
		outF.write(outBuf,extra * 2);
		inF.close();
	}
	delete buffer;
	delete outBuf;
	outF.close();
}


//==============================================================================//

int checkForDuplicates( char *argv[] )
{
	int hFlag=0,fFlag=0;
	while(1) {
		if(*argv[0] == '-') 
		{
			if(*(argv[0]+1) == 'f' || *(argv[0]+1) == 'F') fFlag++;
			if(*(argv[0]+1) == 'h' || *(argv[0]+1) == 'H') hFlag++;
		}
		argv++;
		if(*argv == NULL) break;
	}
	if(fFlag > 1 || hFlag > 1)
		return 0;
	return 1;
}

//==============================================================================//

void argParser( int argc, char *argv[] )
{ 
	if(!checkForDuplicates(argv)) 
	{
		displayHelp(argv[0]);
		return;
	}
	if(argv[1][0] == '-') 
	{
		switch(argv[1][1]) 
		{
			case 'f':
			case 'F': processFile(argv[2]); break;
			case 'h': 
			case 'H': 
			default: displayHelp(argv[0]); break;
		}
	} 
	else 
	{
		displayHelp(argv[0]);
	}
}

//==============================================================================//
// Main program starts here
//==============================================================================//

int main( int argc, char *argv[] )
{
	if(argc < 2) {
		displayUsagePattern(argv[0]);
		return -1;
	} 
	else 
	{
		argParser(argc, argv);
	}
	return 0;
}

//==============================================================================//
// End of File
//==============================================================================//
