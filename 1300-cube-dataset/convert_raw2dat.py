import os,sys,datetime
import numpy as np

try:
	filepath = sys.argv[1]
except:
	sys.exit("\n\nERROR :: please run script w/ fullpath of target .raw file as argv\n")

### Spliting filepath into important naming 
filename = filepath.split("/")[-1]
fullpath = filepath.split("/%s" % (filename))[0]
sample = filename.split("_")[0]
voxel = filename.split("_")[1]
ndims = filename.split("_")[-1].split(".raw")[0]
nx = ndims.split("x")[0]
ny = ndims.split("x")[1]
nz = ndims.split("x")[2]

### Loading segmented 8bit file
load_data = np.fromfile(filepath,'uint8')
print("\nStep 1) Loaded .raw into python")

### Clear previous .dat
dat_file = "%s_%s_%sx%sx%s.dat" % (sample,voxel,nx,ny,nz)
dat_path = "%s/%s" % (fullpath,dat_file)
open_dat = open(dat_path,"w")
open_dat.close()

### Loop through data and build the string
total_points = int(nx) * int(ny) * int(nz)
current_point = 0
incremental_percent = 0
data_string = ""
start_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
print("Step 2) Building .dat string ... (started at %s)" % (start_date))
for i in load_data:
	current_point += 1
	percent_complete = int((float(current_point) / float(total_points)) * float(100))
	if percent_complete > incremental_percent:
		now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
		sys.stdout.write("\r\t%.0f%% at %s" % (percent_complete,now))
		sys.stdout.flush()
		incremental_percent += 1
	data_string += "%s\n" % (i)
	

### Print to the .dat
open_dat = open(dat_path,"a")
open_dat.write(data_string)
open_dat.close()
	
	
