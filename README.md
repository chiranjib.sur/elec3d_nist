# Elec3D : An electrical conductivity calculator 

**Elec3D** - This is a Fortran based solver for  Laplace's equation in a random conducting material using the finite element method.

             This is an optimzied version and the baseline code was obtained from NIST

             This version can read the *RAW* file directly with the support of handing *DAT* file for backward compatibility
             
***References***:
    
             * Images and associated data [https://github.com/cageo/Krzikalla-2012]
             * Link to the paper: [https://www.sciencedirect.com/science/article/pii/S0098300412003172] 



 
 **Directory Structure**
 

   **`README.md`**                  : This file, contains the generic description about this program
   
   **`setup.intel`**                : Set up script to use Intel compiler and tool chain (invoke as : bash$ source ./setup.intel) [Shell's compute environment]
   
   **`setup.OpenMPI`**              : Set up script to use OpenMPI/GNU compiler and tool chain (invoke as : bash$ source ./setup.OpenMPI) [Shell's compute environment]
   
   **`src`**                        : All the source code - both base version (elecfem3d_mpi_base.f) and the optimized version (elecfem3d_mpi_optimized.f) + the stand 
                                      alone utility of RAW to DAT file converter (r2d.cpp)
   
   **`bin`**                        : All the binaries (executables) are stored here
   
   **`Makefile.intel`**             : Makefile to use using Intel MPI and Intel compiler/toolchain (invoke like : $make -f Makefile.intel)
   
   **`Makefile.ompi`**              : Makefile to use using OpenMPI (3.1.4) and GNU tool chain (invoke like : $make -f Makefile.ompi)
   
   **`1300-cube-dataset`**          : Sample data set (.dat files)
   
   **`modernized-testing`**         : Testing data using the optimized version of the code. You need to add following command before running the intel version of the executable. **"ulimit -s unlimited"**
   
   **`modernized-testing/profile`** : Profile data of the code (to understand the runtime performance)
   


-----------------------------------------------


***Compilation Instructions***


 Use the **elec3d_source_code** name accordingly for Elec3D: 
     base version          == elecfem3d_mpi_base.f
     optimized version     == elecfem3d_mpi_optimized.f
      

***OpenMPI Compilers / GNU Tool chain :***

    Use of "Makefile.ompi" recommended - Usage details : $make -f Makefile.omp

	Elec3D  : mpif90 -ffixed-line-length-150 <**elec3d_source_code**> -o elecfem3d_mpi_optimized.openmpi
    RAW2DAT : icc -ffast-math r2d.cpp -o raw2dat.intel
 
  
 ***Intel Compilers / Intel Tool chain :*** 
 
    Use of "Makefile.intel" recommended - Usage details : $make -f Makefile.intel

    Elec3D  : mpiifort -extend-source 132 -fp-model precise -fc=ifort -f90=ifort <**elec3d_source_code**> -o elecfem3d_mpi_optimized.intel
    RAW2DAT : icc -ffast-math r2d.cpp -o raw2dat.intel
 
 
 
***Execution Instructions :***


*Execution sequence:*

Elec3D execution has an optinal flag (*Image file name*). By using this flag the user will be able to control the name of the image file (either in .dat or in .raw)
and bypass the declaration of the same in the .pam file. This command line parameter will have highest precedence.

This **optional flag** is the RAW or DAT file name

<executable_name> : ***elecfem3d_mpi_optimized.openmpi*** OR ***elecfem3d_mpi_optimized.intel***
 
    $ mpirun -np <number_of_processes> ./<executable_name>  elecfem3d_mpi.pam <Image file name>

    Example : $ mpirun -np 8 elecfem3d_mpi_optimized.intel elecfem3d_mpi.pam Imagefile.raw


**Prerequisites**:


Create a file called *elecfem3d_mpi.pam* and add below lines to the created file.

For 51x51x51 Grid Size:

    test.dat               : Input image in .dat format
    test                   : Output Directory  (this is not is use; kept for backward compatibiilty)
    51 51 51               : nx, ny, nz 
    1.0e-12                : Conv criteria 
    0                      : Write Stress (0=no;1=yes) 
    2                      : No of Phase 
    2.899                  : Conductivity (phase 0 - Brine) 
    1.389e-18              : Conductivity (phase 1 - Quartz) 
    1.0 1.0 1.0            : Applied electric field 
 
For 512x512x512 Grid Size:

    CG1M1_512.dat          : Input image in .dat format
    CG1M1_512              : Output Directory (this is not is use; kept for backward compatibiilty)
    512 512 512            : nx, ny, nz 
    1.0e-7                 : Conv criteria  
    0                      : Write Stress (0=no;1=yes) 
    2                      : No of Phase 
    5.000                  : Conductivity (phase 0 - Brine) 
    1.024e-006             : Conductivity (phase 1 - Quartz)
    1.0 1.0 1.0            : Applied electric field

***Note*** : Make sure that your *current working directory* should contain **elecfem3d_mpi.pam**
             
             Declaring the image file name **test.dat** within the .pam file is optional.
             
             Output directory will be created based on the image file name 
             (if the image file is **Image_test.raw**, the output directory will be **Image_test**)
             
             The structure of the *.pam* file is kept as the same.
             to address the backward compatibility of the code with existing input files

 
For more details : Contact - Chiranjib Sur <`Chiranjib.Sur@Shell.Com`>

Last Modified : *September 7, 2020*